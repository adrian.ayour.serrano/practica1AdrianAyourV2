CREATE SCHEMA product;
	CREATE TABLE IF NOT EXISTS product.product (
		brand_id int4 NULL,
		start_date timestamp NULL,
		end_date timestamp NULL,
		price_list int4 NULL,
		product_id int4 NOT NULL,
		priority int4 NULL,
		price float8 NULL,
		curr varchar NULL
	);


insert into product.product (brand_id, start_date, end_date, price_list, product_id, priority, price, curr) values (1,'2020-06-14 00:00:00.000','2020-12-31 23:59:59.000', 1, 35455, 0, 35.50,'EUR');
insert into product.product (brand_id, start_date, end_date, price_list, product_id, priority, price, curr) values (1,'2020-06-14 15:00:00.000','2020-06-14 18:30:00.000', 2, 35455, 1, 25.45,'EUR');
insert into product.product (brand_id, start_date, end_date, price_list, product_id, priority, price, curr) values (1,'2020-06-15 00:00:00.000','2020-06-15 11:00:00.000', 3, 35455, 1, 30.50,'EUR');
insert into product.product (brand_id, start_date, end_date, price_list, product_id, priority, price, curr) values (1,'2020-06-15 16:00:00.000','2020-12-31 23:59:59.000', 4, 35455, 1, 38.95,'EUR');

