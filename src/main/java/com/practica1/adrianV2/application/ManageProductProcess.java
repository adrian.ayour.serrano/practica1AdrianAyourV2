package com.practica1.adrianV2.application;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.practica1.adrianV2.model.Product;
import com.practica1.adrianV2.persistence.ProductRepository;
import com.practica1.adrianV2.ui.ProductDTO;
import com.practica1.adrianV2.ui.ProductPetitionDTO;


@Service
public class ManageProductProcess{

	ProductRepository productRepository;

	ModelMapper mapper;
		
	public ManageProductProcess(ProductRepository productRepository, ModelMapper mapper) {
		this.productRepository = productRepository;
		this.mapper = mapper;
	}
	
	public ProductDTO manageProducts(ProductPetitionDTO productPetitionDTO) {
		
		Product product = new Product();
		mapper.map(productPetitionDTO, product);
			
		List<Product> listProduct = this.productRepository.findByBrand_idAndProduct_idOrderByPriorityAndPrice(product.getBrand_id(), product.getProduct_id());	 
		ProductDTO productDTO = new ProductDTO();				
		productDTO = product.getProductDTO(listProduct, productPetitionDTO.getApplicationDate());
		
		return productDTO;
	}
	
}
