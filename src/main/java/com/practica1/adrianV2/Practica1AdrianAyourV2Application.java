package com.practica1.adrianV2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.practica1.adrianV2.persistence")
@EntityScan("com.practica1.adrianV2.*")  
@ComponentScan(basePackages = {"com.practica1.adrianV2.*"})
public class Practica1AdrianAyourV2Application {

	public static void main(String[] args) {
		SpringApplication.run(Practica1AdrianAyourV2Application.class, args);
	}

}
