package com.practica1.adrianV2.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.practica1.adrianV2.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

	@Query(value="select * From public.product p where p.brand_id = ?1 and p.product_id = ?2 order by p.priority desc, p.price desc", nativeQuery = true)
	List<Product> findByBrand_idAndProduct_idOrderByPriorityAndPrice(long brandId, long id);
}
