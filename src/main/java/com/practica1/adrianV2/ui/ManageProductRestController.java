package com.practica1.adrianV2.ui;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practica1.adrianV2.application.ManageProductProcess;


@RestController
@RequestMapping("/products")
public class ManageProductRestController {

	private final ManageProductProcess manageProductProcess;

	public ManageProductRestController(ManageProductProcess manageProductProcess) {
		this.manageProductProcess = manageProductProcess;
	}

	@GetMapping("/check")
	public ProductDTO manageProduct(@RequestBody ProductPetitionDTO productPetitionDTO)
	{
		return manageProductProcess.manageProducts(productPetitionDTO);
	}

}
