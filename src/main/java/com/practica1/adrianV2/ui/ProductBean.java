package com.practica1.adrianV2.ui;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductBean {
	
	@Bean
	public ModelMapper modelMapper() {
	  return new ModelMapper();
	}
	
}
