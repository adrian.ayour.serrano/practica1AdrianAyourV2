package com.practica1.adrianV2.ui;

import lombok.Data;

@Data
public class ProductPetitionDTO {

	private String applicationDate;
	private long product_id;
    private long brand_id;
}
