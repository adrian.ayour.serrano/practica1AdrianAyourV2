package com.practica1.adrianV2.ui;

import lombok.Data;

@Data
public class ProductDTO {

	private long product_id;
    private long brand_id;
    private int price_list;
    private String start_date;
    private String end_date;
    private float price;
    

}
