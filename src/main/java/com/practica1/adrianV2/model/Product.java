package com.practica1.adrianV2.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.practica1.adrianV2.ui.ProductDTO;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name = "product", schema = "product")
public class Product  {

	private long brand_id;
	private LocalDateTime start_date;
	private LocalDateTime end_date;
	@Id	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int price_list;
	private long product_id;
	private int priority;
	private float price;
	private String curr;

	public LocalDateTime parseDates(String date) {

		DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd").appendPattern(" HH:mm:ss.SSS").toFormatter();
		LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);

		return localDateTime;
	}

	public ProductDTO getProductDTO(List<Product> listProduct, String date) {
		ProductDTO productDTO = new ProductDTO();
		ModelMapper mapper = new ModelMapper();
		List<Product> listWithDatesFiltered = new ArrayList<>();
		for (int i = 0; i < listProduct.size(); i++) {		
			if(listProduct.get(i).getStart_date().isBefore(this.parseDates(date)) && listProduct.get(i).getEnd_date().isAfter(this.parseDates(date))) {
				listWithDatesFiltered.add(listProduct.get(i));
			}
		}
		
		if(!listWithDatesFiltered.isEmpty()) {
			mapper.map(listWithDatesFiltered.get(0), productDTO);			
		}
		
		return productDTO;
	}

}
