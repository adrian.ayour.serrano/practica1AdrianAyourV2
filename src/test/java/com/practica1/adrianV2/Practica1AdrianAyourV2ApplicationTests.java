package com.practica1.adrianV2;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.practica1.adrianV2.application.ManageProductProcess;
import com.practica1.adrianV2.persistence.ProductRepository;
import com.practica1.adrianV2.ui.ProductDTO;
import com.practica1.adrianV2.ui.ProductPetitionDTO;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Practica1AdrianAyourV2ApplicationTests {

	@Autowired
	ManageProductProcess manageProductProcess;	
	@Autowired
	ProductRepository productRepository;
	@Autowired
	ModelMapper mapper;

	@Before
	public void init() {
		manageProductProcess = new ManageProductProcess(productRepository, mapper);
	}
	
	@Test
	public void manage_Day14_Hour10_00_Product35455_Brand1() {

		//Given
		ProductPetitionDTO productPetitionDTO = new ProductPetitionDTO();
		productPetitionDTO.setBrand_id(1);
		productPetitionDTO.setApplicationDate("2020-06-14 10:00:00.000");
		productPetitionDTO.setProduct_id(35455);
		
		//When
		ProductDTO productDTO = manageProductProcess.manageProducts(productPetitionDTO);
		
		//Then
		assertTrue(productDTO.getPrice() != 0);
	}
	
	@Test
	public void manage_Day14_Hour16_00_Product35455_Brand1() {
		
		//Given
		ProductPetitionDTO productPetitionDTO = new ProductPetitionDTO();
		productPetitionDTO.setBrand_id(1);
		productPetitionDTO.setApplicationDate("2020-06-14 16:00:00.000");
		productPetitionDTO.setProduct_id(35455);
		
		//When
		ProductDTO productDTO = manageProductProcess.manageProducts(productPetitionDTO);
		
		//Then
		assertTrue(productDTO.getPrice() != 0);
	}
	
	@Test
	public void manage_Day14_Hour21_00_Product35455_Brand1() {
		//Given
		ProductPetitionDTO productPetitionDTO = new ProductPetitionDTO();
		productPetitionDTO.setBrand_id(1);
		productPetitionDTO.setApplicationDate("2020-06-14 21:00:00.000");
		productPetitionDTO.setProduct_id(35455);
		
		//When
		ProductDTO productDTO = manageProductProcess.manageProducts(productPetitionDTO);
		
		//Then
		assertTrue(productDTO.getPrice() != 0);
	}
	
	@Test
	public void manage_Day15_Hour10_00_Product35455_Brand1() {
		//Given
		ProductPetitionDTO productPetitionDTO = new ProductPetitionDTO();
		productPetitionDTO.setBrand_id(1);
		productPetitionDTO.setApplicationDate("2020-06-15 10:00:00.000");
		productPetitionDTO.setProduct_id(35455);
		
		//When
		ProductDTO productDTO = manageProductProcess.manageProducts(productPetitionDTO);
		
		//Then
		assertTrue(productDTO.getPrice() != 0);
	}
	
	@Test
	public void manage_Day16_Hour21_00_Product35455_Brand1() {
		//Given
		ProductPetitionDTO productPetitionDTO = new ProductPetitionDTO();
		productPetitionDTO.setBrand_id(1);
		productPetitionDTO.setApplicationDate("2020-06-16 21:00:00.000");
		productPetitionDTO.setProduct_id(35455);
		
		//When
		ProductDTO productDTO = manageProductProcess.manageProducts(productPetitionDTO);
		
		//Then
		assertTrue(productDTO.getPrice() != 0);
	}

}
